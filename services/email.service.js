const nodemailer = require("nodemailer");

// async..await is not allowed in global scope, must use a wrapper
async function main(recive, subject, text) {
    // console.log(recive, subject, text);
    let transporter = nodemailer.createTransport({
        host: "smtp.hostinger.in.th",
        port: 587,
        secure: false, // true for 465, false for other ports
        auth: {
            user: "angkura@moocrepositiry.com", // generated ethereal user
            pass: "Pass@2471", // generated ethereal password
        },
    });
    let info = await transporter.sendMail({
        from: '"Fred Foo " <angkura@moocrepositiry.com>', // sender address
        to: recive + ", " + recive, // list of receivers
        // to: `${recive}, ${recive}`, // list of receivers
        subject, // Subject line
        text, // plain text body
        html: "<b>" + text + "</b>", // html body
    });

    console.log("Message sent: %s", info.messageId);
    // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
    return info 
    // Preview only available when sending through an Ethereal account
    // console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
    // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
}

module.exports = { main }