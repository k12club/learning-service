const mongoose = require('mongoose')

const userScema = mongoose.Schema({
    email: {
        type: String,
        required: true,
        unique: true,
        match: /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/ // regex
    },
    password: {
        type: String,
        required: true,
        // match:/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/ //regex
    }
})

module.exports = mongoose.model("User", userScema)