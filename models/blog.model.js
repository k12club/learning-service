// import mongoose from 'mongoose';
var mongoose = require('mongoose')
const { Schema } = mongoose;

const blogSchema = new Schema({
    title: String, // String is shorthand for {type: String}
    comments: [{ body: String, date: Date }],
    date: { type: Date, default: Date.now },
    meta: {
        votes: Number,
    },
    filename: String
});

const Blog = mongoose.model('Blog', blogSchema);

module.exports = Blog