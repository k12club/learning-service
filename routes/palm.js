var express = require('express');
var router = express.Router();
var Blog = require('../models/blog.model')
var multer = require('multer')

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './uploads/');
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + file.originalname);
    }
});

const fileFilter = (req, file, cb) => {
    // reject a file
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
        cb(null, true);
    } else {
        cb(null, false);
    }
};

const upload = multer({
    storage: storage,
    limits: {
        fileSize: 1024 * 1024 * 5
    },
    fileFilter: fileFilter
});



/* GET palms listing. */
router.get('/', async function (req, res) {
    const doc = await Blog.find()
    res.json(doc)
});


/* GET palms by id listing. */
router.get('/:id', async function (req, res, next) {
    let id = req.params.id
    const doc = await Blog.findOne({ _id: id })
    res.json(doc)
});


/* POST palms listing. */
router.post('/upload',
    upload.single('file'),
    async function (req, res, next) {
        let body = req.body
        let param = req.file
        const doc = new Blog({ ...body, filename: param.filename });
        await doc.save()

        res.json({
            ...doc._doc,
            url: "http://127.0.0.1:3000/uploads/" + param.filename
        })
    });


/* POST palms listing. */
router.post('/', async function (req, res, next) {
    let body = req.body
    const doc = new Blog(body);
    await doc.save()

    res.json(doc)
});



/* put palms listing. */
router.put('/:id', async function (req, res, next) {
    let id = req.params.id
    let body = req.body

    // let title = req.body.title
    console.log(id);
    let old_result = await Blog.findOneAndUpdate({ _id: id }, body)
    let getUpdate = await Blog.findById({ _id: id })
    res.json({ old_result, getUpdate })
});

/* delete palms listing. */
router.delete('/:id', async function (req, res, next) {
    let id = req.params.id
    if (id) {
        let value = await Blog.findByIdAndDelete({ _id: id })
        res.status(200).json(value)
    } else {

        res.status(200).json('login fail')
    }
});


module.exports = router;
