var express = require('express');
var router = express.Router();
var bcrypt = require('bcrypt')
var User = require('../models/user.model')
var email = require('../services/email.service')
var jwt = require('jsonwebtoken')
/* GET users listing. */
router.get('/', function (req, res, next) {
  res.send('respond with a resource');
});

router.post('/register', (req, res) => {
  User.find({ email: req.body.email })
    .exec()
    .then(user => {
      if (user.length >= 1) {
        return res.json('mail already use')
      } else {
        bcrypt.hash(req.body.password, 10, (err, hash) => {
          if (err) {
            return res.json(err);
          } else {
            const user = new User({
              email: req.body.email,
              password: hash
            });
            user
              .save()
              .then(result => {
                let send_email = email.main(result.email, 'สมัครสมาชิก', 'ลงทะเบียนสำเร็จแล้ว')
                res.json({ result, ...send_email })
              }).catch(err => {
                console.log(err);
              });
          };
        });
      };
    });
});

router.post('/login', (req, res) => {
  User.findOne({ email: req.body.email })
    .exec()
    .then(user => {
      console.log(user, 'user');
      if (!user) {
        // let send_email = email.main(user.email, 'เข้าสู่ระบบผิดพลาด', 'มีคนกำลังพยายามเข้าถึงบัญชีผู้ใช้งานของคุณ')
        let message = 'Auth Failed'
        return res.json({ message })
      } else {
        bcrypt.compare(req.body.password, user.password, (err, result) => {
          if (!result) {
            let send_email = email.main(user.email, 'เข้าสู่ระบบผิดพลาด', 'มีคนกำลังพยายามเข้าถึงบัญชีผู้ใช้งานของคุณ')
            let message = 'Auth Failed'
            return res.json({ message, send_email })
          } else {
            let send_email = email.main(user.email, 'เข้าสู่ระบบสำเร็จ', 'ลงชื่อเข้าใช้สำเร็จแล้ว')
            let message = 'Auth Successful'
            const token = jwt.sign(
              {
                email: user.email,
                _id: user._id,
                name: 'palm',
                send_email
              },
              "catanddog",
              {
                expiresIn: "1h"
              }
            )
            return res.json({
              message,
              token
            })
          }
        })
      }
    });
});

module.exports = router;
